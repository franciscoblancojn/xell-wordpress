# Changelog

All notable changes to this project will be documented in this file.

## [v1.1.2](https://gitlab.com/franciscoblancojn/xell-wordpress/-/tree/f75930c2a4ebc97b387012e688354fa9a31c4ca8)

### New
- Add Button disconnect

### Update
- Remove token of url in connect Xell

## [v1.1.1](https://gitlab.com/franciscoblancojn/xell-wordpress/-/tree/c223995ddff72e0999978da486c4da25bfdb47d2)

### New
- Add tutorial for install and connetc plugin

## [v1.1.0](https://gitlab.com/franciscoblancojn/xell-wordpress/-/tree/3d953c98b45115820b7f8249951221d13cdfd7c9)

### Update
- Change save token and active "Connect Woocommerce with your api"

## [v1.0.4](https://gitlab.com/franciscoblancojn/xell-wordpress/-/tree/912069ae911aa3331e614a7a14689ed8328f6ba0)

### New
- Add Dependences "Woocommerce" and "Connect Woocommerce with your api"

## [v1.0.3](https://gitlab.com/franciscoblancojn/xell-wordpress/-/tree/b50108b4aeb42571d155d4e84bbb6771d3123ed9)

### New
- Connect Xell and save token

## [v1.0.2](https://gitlab.com/franciscoblancojn/xell-wordpress/-/tree/1e6548f293ffab2d14beb029b6a01b6df55815ab)

### New
- Connect with Gitlab

## [v1.0.1](https://gitlab.com/franciscoblancojn/xell-wordpress/-/tree/cf3d4b71374e023809477bf66b0c60e2136afc77)

### New
- Page Config

## [v1.0.0](https://gitlab.com/franciscoblancojn/xell-wordpress/-/tree/10df06cb1be2b53a5b1bd489fedc568d61c76270)

### New
- Init Plugin