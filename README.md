# Xell

Xell es un plugin que usa el plugin "Connect Woocommerce with your api" para conectar tu Sitio con Xell.

- [Installing](#installing)
- [Connect](#connect)
- [Developer](#developer)
- [Repositories](#repositories)

# Installing

1) Download file in [here](https://gitlab.com/franciscoblancojn/xell-wordpress/-/tree/master)

![alt download](https://gitlab.com/franciscoblancojn/xell-wordpress/-/raw/develop/tutorial/download.png)

2) Go to Dashboard in Wordpress 

3) Go to Plugins > Add New > Upload Plugin 

![alt install](https://gitlab.com/franciscoblancojn/xell-wordpress/-/raw/develop/tutorial/install.png)

4) Upload File > Active Plugin 

![alt active](https://gitlab.com/franciscoblancojn/xell-wordpress/-/raw/develop/tutorial/active.png)

# Connect

1) Go to Dashboard in Wordpress 

2) Go to Xell and Connect

![alt connect](https://gitlab.com/franciscoblancojn/xell-wordpress/-/raw/develop/tutorial/connect.png)




## Developer
[Francisco Blanco](https://franciscoblanco.vercel.app/)

[Gitlab franciscoblancojn](https://gitlab.com/franciscoblancojn)

[Email blancofrancisco34@gmail.com](mailto:blancofrancisco34@gmail.com)

## Repositories

- [Gitlab](https://gitlab.com/franciscoblancojn/xell-wordpress/-/tree/master)