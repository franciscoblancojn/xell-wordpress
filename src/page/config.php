<?php
function XELL_create_menu() {
	//create new top-level menu
	add_menu_page('XELL Settings', 'Xell', 'administrator', "XELL_config", 'XELL_settings_page' ,XELL_URL."src/img/icon.png" );

	//call register settings function
	add_action( 'admin_init', 'register_XELL_settings' );
}
add_action('admin_menu', 'XELL_create_menu');


function register_XELL_settings() {
	//register our settings
	register_setting( 'XELL-settings-group', 'new_option_name' );
	register_setting( 'XELL-settings-group', 'some_other_option' );
	register_setting( 'XELL-settings-group', 'option_etc' );
}

function XELL_saveApi($token)
{
    if($token  != "" && is_string($token)){
        $config = array(
            "name"          => "XELL",
            "url"           => XELL_URL_API,
            "token"         => $token,
            "permission"    => array(
                "product_ready"     => true,
                "product_create"    => true,
                "product_update"    => true,
                "product_delete"    => true,
                "order_ready"       => true,
                "order_create"      => true,
                "order_update"      => true,
                "order_delete"      => true,
                "user_ready"        => true,
                "user_create"       => true,
                "user_update"       => true,
                "user_delete"       => true,
            ),
        );
	    $apis = CWWYA_get_option("apis");
        for ($i=0; $i < count($apis); $i++) { 
            if($apis[$i]["name"] == "XELL"){
                CWWYA_setApiByName("XELL",$config);
                return;
            }
        }
        CWWYA_addApi($config);
        CWWYA_active();
    }
}
function XELL_getTokenApi(){
    $apis = CWWYA_get_option("apis");
    for ($i=0; $i < count($apis); $i++) { 
        if($apis[$i]["name"] == "XELL"){
            return $apis[$i]["token"] ;
        }
    }
    return "";
}

function XELL_settings_page() {
    if($_GET["token"]){
        XELL_saveApi($_GET["token"]);
        ?>
        <script>
            window.location = `<?=menu_page_url("XELL_config",false)?>`
        </script>
        <?php
        exit;
    }
    if($_GET["error"]){
        CWWYA_deleteApiByName("XELL");
    }
    if($_GET["disconnect"]){
        CWWYA_deleteApiByName("XELL");
    }

    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')   
         $url = "https://";   
    else  
         $url = "http://";   
    // Append the host(domain name, ip) to the URL.   
    $url.= $_SERVER['HTTP_HOST'];   
    // Append the requested resource location to the URL   
    $url.= $_SERVER['REQUEST_URI'];    

    $token = XELL_getTokenApi();

    $name = get_bloginfo();

    ?>
	<link rel="stylesheet" href="<?=XELL_URL?>src/css/page-config.css?v=<?=XELL_get_version()?>">
    <div class="wrap page-config-XELL">
		<form method="get" class="form-login" action="<?=XELL_URL_CONNECT?>">
            <input type="hidden" name="name" value="<?=$name?>">
            <input type="hidden" name="home" value="<?=get_home_url()?>">
            <input type="hidden" name="logo" value="<?=get_site_icon_url()?>">
            <input type="hidden" name="url" value="<?=$url?>">
            <input type="hidden" name="api" value="<?=CWWYA_URL?>">

            <img class="logo" src="<?=XELL_URL?>src/img/logo.png" alt="">
            <?php
                if($_GET["error"]){
                    ?>
                        <img class="status" src="<?=XELL_URL?>src/img/x.png" alt="">
                        <div class="msg-error msg-status"><?=$_GET["error"]?></div>
                    <?php
                }else{
                    if($token  != "" && is_string($token)){
                        ?>
                            <img class="status" src="<?=XELL_URL?>src/img/ok.png" alt="">
                            <div class="msg-ok msg-status">Xell Conectado</div>
                            <input type="hidden" name="disconnect" value="true">
                            <button class="btn-disconnect">Desconectar Xell</button>
                        <?php
                    }else{
                        ?>
                            <button class="btn-connect">Conectar Xell</button>
                        <?php
                    }
                }
            ?>
		</form>
    </div>
    <?php 
}