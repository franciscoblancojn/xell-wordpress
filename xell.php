<?php
/*
Plugin Name: Xell
Plugin URI: https://gitlab.com/franciscoblancojn/xell-wordpress
Description: Xell es un plugin que usa el plugin "Connect Woocommerce with your api" para conectar tu Sitio con Xell.
Author: franciscoblancojn
Version: 1.1.8
Author URI: https://franciscoblanco.vercel.app/
License: ISC
 */

if (!function_exists( 'is_plugin_active' ))
    require_once( ABSPATH . '/wp-admin/includes/plugin.php' );

if(!is_plugin_active( 'woocommerce/woocommerce.php' )){
    function XELL_log_dependencia() {
        ?>
        <div class="notice notice-error is-dismissible">
            <p>
                Xell requiere the plugin  "Woocommerce"
            </p>
        </div>
        <?php
    }
    add_action( 'admin_notices', 'XELL_log_dependencia' );
}else if(!is_plugin_active( 'connect-woocommerce-with-your-api/connect-woocommerce-with-your-api.php' ) && !is_plugin_active( 'connect-woocommerce-with-your-api-master/connect-woocommerce-with-your-api.php' )){
    function XELL_log_dependencia() {
        ?>
        <div class="notice notice-error is-dismissible">
            <p>
                Xell requiere the plugin  "Connect Woocommerce with your api", install <a href="https://gitlab.com/franciscoblancojn/connect-woocommerce-with-your-api/-/tree/master" target="_blank" rel="noopener noreferrer">here</a>
            </p>
        </div>
        <?php
    }
    add_action( 'admin_notices', 'XELL_log_dependencia' );
}else{
    require 'plugin-update-checker/plugin-update-checker.php';
    $myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
        'https://gitlab.com/franciscoblancojn/xell-wordpress',
        __FILE__,
        'xell-wordpress'
    );
    $myUpdateChecker->setAuthentication('MqiAAZzo9WNBAGzgwc3L');
    $myUpdateChecker->setBranch('master');

    function XELL_get_version() {
        $plugin_data = get_plugin_data( __FILE__ );
        $plugin_version = $plugin_data['Version'];
        return $plugin_version;
    }

    define("XELL_LOG",false);
    define("XELL_PATH",plugin_dir_path(__FILE__));
    define("XELL_URL",plugin_dir_url(__FILE__));
    define("XELL_URL_API","http://localhost:3005");
    define("XELL_URL_CONNECT","http://localhost:4200/configuraciones/cms/wordpress/connect/api");

    require_once XELL_PATH . "src/_index.php";
}
